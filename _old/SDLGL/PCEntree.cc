#include "PCEntree.h"

namespace ProjetEPFL
{

PCEntree::PCEntree () : quit(false)
{
	memset(this,0,sizeof(*this));
}

void PCEntree::miseAJour ()
{
	SDL_Event event ;
	
	while(SDL_PollEvent(&event))
	{
		switch ( event.type )
		{
			case SDL_KEYDOWN :
				key[event.key.keysym.sym] = 1 ;
				break ;
			case SDL_KEYUP :
				key[event.key.keysym.sym] = 0 ;
				break ;
			case SDL_MOUSEMOTION :
				mouse.x = event.motion.x ;
				mouse.y = event.motion.y ;
				mouseRel.x = event.motion.xrel ;
				mouseRel.y = event.motion.yrel ;
				break ;
			case SDL_MOUSEBUTTONDOWN :
				mouseButton[event.button.button] = 1 ;
				break ;
			case SDL_MOUSEBUTTONUP :
				mouseButton[event.button.button] = 0 ;
				break ;
			case SDL_QUIT :
				quit = true ;
				break ;
			default :
				break ;
		}
	}
}


bool PCEntree::KeyPressed ()
{
    for ( unsigned int i(0) ; i < SDLK_LAST ; i ++ )
    {
        if ( key[i] )
            return true ;
    }

    return false ;
}

}
