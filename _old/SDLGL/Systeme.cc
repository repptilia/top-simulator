#include "Systeme.h"

namespace ProjetEPFL
{

//	Constructeurs

Systeme::Systeme () : std::vector<Toupie*>(), g(9.81), om(6), dt(10.0), t(0.0)
{
	if ( SDL_Init ( SDL_INIT_VIDEO ) == -1 ) throw Erreur("SDL_Init() in Systeme::Systeme()",1) ;
	
	if ( SDL_SetVideoMode(800, 600, 32, SDL_OPENGL ) == NULL ) throw Erreur("SDL_SetVideoMode() in Systeme::Systeme()",1) ;
	
	SDL_WM_SetCaption("Système", 0);

	glEnable ( GL_DEPTH_TEST ) ;			// Profondeur
	glEnable ( GL_TEXTURE_2D ) ;			// Textures

	glMatrixMode(GL_PROJECTION) ;			// fixe la perspective
	glLoadIdentity() ;
	gluPerspective(65.0, 4./3., 1.0, 1000.0) ;
	glClearColor(0.0, 0.0, 0.0, 1.0) ;		// fixe la couleur du fond à noir
	
	for ( unsigned int i(0) ; i < NB_TEXTURES ; ++i )
		texture[i] = chargerTexture(std::string("/home/repptilia/Documents/ProjetEPFL/Data/Texture/texture_" + string(i+1) + ".jpg"));
	integrateur = new IntegrateurEuler(dt/1000.0) ;
	
	glMatrixMode(GL_MODELVIEW) ;			// mode de visualisation
}

Systeme::Systeme ( const Systeme &systeme ) : std::vector<Toupie*>()
{
	*this = systeme ;
}

Systeme Systeme::operator= ( const Systeme &systeme )
{
	resize(systeme.size()) ;

	for ( unsigned int i(0) ; i < size() ; ++i )
	{
		*(begin() + i) = new Toupie ;
		*(begin() + i) = *(systeme.begin() + i) ;
	}
	
	integrateur = systeme.integrateur->copie() ;
	
	g	= systeme.g ;
	om	= systeme.om ;
	dt	= systeme.dt ;
	t	= systeme.t ;
	
	for ( unsigned int i(0) ; i < NB_TEXTURES ; ++i )
		texture[i] = systeme.texture[i] ;
	
	return *this ;
}


//	Méthodes
	
void Systeme::affiche ( std::ostream &out ) const
{
	if ( size() > 0 )
	{
		out << "Le système comporte " << size() << " Toupies." << std::endl ;
		
		for ( unsigned int i(0) ; i < size() ; ++i )
			out << "Toupie " << i << " : " << **(begin() + i) << std::endl ;
	}
}

void Systeme::ajoute ( const Toupie& toupie )
{
	push_back(toupie.copie()) ;
}

void Systeme::supprime ()
{
	for ( unsigned int i(0) ; i < size() ; ++i )
		delete *(begin() + i) ;					// Suppression des objets

	clear() ; 								// Suppression des adresses

	//delete integrateur ;						//							/!\	PB DESALLOCATION
}

void Systeme::dessine ( SortieDessin sortie ) const
{
	switch ( sortie )
	{
		case ECRAN :
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// commence par effacer l'ancienne image
			glLoadIdentity() ;

			regarde () ;
			dessineEnvironnement () ;
			
			for ( unsigned int i(0) ; i < size() ; ++i )
			{
				integrateur->integre(*(begin()+i), t) ;
				(*(begin()+i))->dessine() ;
			}
			
			dessinerAxes(5) ;
			 
			  // Finalement, on envoie le dessin à l'écran
			glFlush() ;
			SDL_GL_SwapBuffers() ;
			
			break ;
		
		case CONSOLE :
			if ( size() )
			{
				std::cout << "Le système se dessine :" << std::endl ;
				for ( unsigned int i(0) ; i < size() ; i ++ )
				{
					integrateur->integre(*(begin()+i), t) ;
					std::cout << "Dessin Toupie " << i << " :" << std::endl ;
					(*(begin() + i))->dessine(CONSOLE) ;
					std::cout << std::endl ;
				}
			}
			break ;
		
		default :
			break ;
	}
}

void Systeme::dessineEnvironnement () const
{
									//	DESSIN SOL
	glBindTexture ( GL_TEXTURE_2D, texture[0] ) ;
	glBegin(GL_QUADS) ;
		//glColor3ub(255,0,0) ;
			glTexCoord2i(0,0) ;	glVertex3i(-10,-10,0) ;
		//glColor3ub(0,255,0) ;
			glTexCoord2i(10,0) ;	glVertex3i(10,-10,0) ;
		//glColor3ub(255,255,0) ;
			glTexCoord2i(10,10) ;	glVertex3i(10,10,0) ;
		//glColor3ub(255,0,255) ;
			glTexCoord2i(0,10) ;	glVertex3i(-10,10,0) ;
	glEnd() ;
}

void Systeme::GestionEvenements ()
{
}


int Systeme::active ()
{
	while ( !quit && t < 5001 )
	{
		miseAJour () ;
		GestionEvenements () ;

		dessine () ;

		t += dt ;
		
		SDL_Delay (dt) ; // On attend pour ne pas être surchargé par les évènements
	}

	return 0 ;
}


//	Destructeur

Systeme::~Systeme ()
{
	supprime() ;
	SDL_Quit() ;
}


//	Opérateur externe

std::ostream& operator<< ( std::ostream &out, const Systeme &systeme )
{
	systeme.affiche(out) ;
	return out ;
}

}
