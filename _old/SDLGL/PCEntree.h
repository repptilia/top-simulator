#ifndef PROJETEPFL_H_PCENTREE_INCLUDED
#define PROJETEPFL_H_PCENTREE_INCLUDED

#include <iostream>
#include <SDL/SDL.h>

namespace ProjetEPFL
{

class PCEntree
{
        public :

            //  Constructeur
            PCEntree () ;

            //  Update
            virtual void miseAJour () ;

            //  Status
            virtual bool KeyPressed () ;

            //  Attributs
            char key[SDLK_LAST] ;
            SDL_Rect mouse ;
            SDL_Rect mouseRel ;
            char mouseButton[6] ;
            bool quit ;
            
            //	Destructeur
            ~PCEntree(){} ;
};

}

#endif // PROJETEPFL_PCENTREE_H_INCLUDED
