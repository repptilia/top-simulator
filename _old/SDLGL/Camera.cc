#include "Camera.h"

namespace ProjetEPFL
{

//	Constructeur

Camera::Camera () : sensibiliteMouvement(0.3), sensibiliteMolette(1.0), om(9,0.0)
{
	om[0] = 5.0 ;
}


//	Méthodes

void Camera::miseAJour ()
{
	double sensibiliteClavier(0.3) ;
	PCEntree::miseAJour () ;

	if ( mouseButton[SDL_BUTTON_LEFT] )
	{
        	om[7] += mouseRel.x * sensibiliteMouvement ; //	Mvt Souris X => Rotation horizontale
        	om[6] += mouseRel.y * sensibiliteMouvement ; //	Mvt souris Y => Rotation verticale
        	
        	if ( om[6] > 90 )				// Bridage de l'angle à 90 pour usage plus pratique
            		om[6] = 90 ;
        	else if (om[6] < -90 )
			om[6] = -90 ;
	}

	if ( mouseButton[SDL_BUTTON_RIGHT] )
	{
		oX += mouseRel.x * sensibiliteMouvement * sensibiliteMouvement ;
		oZ += mouseRel.y * sensibiliteMouvement * sensibiliteMouvement ;
		
		pX += mouseRel.x * sensibiliteMouvement * sensibiliteMouvement ;
		pZ += mouseRel.y * sensibiliteMouvement * sensibiliteMouvement ;
	}
	if ( key[SDLK_UP] )
	{
		pZ += sensibiliteClavier ;
		oZ += sensibiliteClavier ;
	}
	if ( key[SDLK_DOWN] )
	{
		pZ -= sensibiliteClavier ;
		oZ -= sensibiliteClavier ;
	}
	if ( key[SDLK_RIGHT] )
	{
		pY += sensibiliteClavier ;
		oY += sensibiliteClavier ;
	}
	if ( key[SDLK_LEFT] )
	{
		pY -= sensibiliteClavier ;
		oY -= sensibiliteClavier ;
	}
	/*
	if ( mouseButton[SDL_BUTTON_MIDDLE] )
	{
		if ( 	DISTANCE  < ZOOM_DISTANCE_MAXIMUM 
		&& 	DISTANCE > ZOOM_DISTANCE_MINIMUM )
			pX += mouseRel.x * sensibiliteMouvement ;

		if (	pY + mouseRel.y * sensibiliteMouvement - oY < ZOOM_DISTANCE_MAXIMUM 
		&& 	pY + mouseRel.y * sensibiliteMouvement - oY > ZOOM_DISTANCE_MINIMUM )
			pY += mouseRel.y * sensibiliteMouvement ;
	}*/
/*
	if ( key[SDLK_UP] )	theta += 2.0 ;
	if ( key[SDLK_DOWN] )	theta -= 2.0 ;
	if ( key[SDLK_RIGHT] )	phi -= 2.0 ;
	if ( key[SDLK_LEFT] )	phi += 2.0 ;
	
	while ( theta < -180.0 ) 	{ theta += 360.0; }
	while ( theta >  180.0 ) 	{ theta -= 360.0; }
	while ( phi <   0.0 ) 		{ phi += 360.0; }
	while ( phi > 360.0 ) 		{ phi -= 360.0; }*/
}

void Camera::regarde () const
{
	gluLookAt(	pX, pY, pZ,
			oX, oY, oZ,
			0,0,1) ;
	glTranslated(-oX, -oY, -oZ);
	glRotated(om[6],0,1,0);
	glRotated(om[7],0,0,1);
}


//	Destructeur

Camera::~Camera()
{/*
    SDL_FreeCursor(cur[0]) ;
    SDL_FreeCursor(cur[1]) ;
    SDL_SetCursor(NULL)	;*/	//	Curseur par défaut
}


}
