#include <sstream>
#include <cstring>
#include <cstdlib>
#include <SDL/SDL_image.h>

#include "Dessinable.h"

namespace ProjetEPFL
{

std::string string ( int i ) ;
SDL_Surface * flipSurface ( SDL_Surface * surface ) ;
GLuint chargerTexture ( std::string texture, bool MipMap = true ) throw(Erreur) ;
void dessinerAxes ( double echelle = 1.0 ) ;
int initFullScreen(unsigned int * width,unsigned int * height) ;
int XPMFromImage(const char * imagefile, const char * XPMfile) ;
SDL_Cursor * cursorFromXPM(const char * xpm[]) ;

}
