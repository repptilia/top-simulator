#ifndef PROJETEPFL_CAMERA_H_INCLUDED
#define PROJETEPFL_CAMERA_H_INCLUDED

#include <GL/gl.h>
#include <GL/glu.h>

#include "Vecteur.h"
#include "PCEntree.h"
#include "FonctionDessin.h"

#define	NB_CURSEURS		2

#define	ZOOM_DISTANCE_MAXIMUM	20
#define	ZOOM_DISTANCE_MINIMUM	1

#define	pX	om[0]
#define	pY	om[1]
#define	pZ	om[2]
#define	oX	om[3]
#define	oY	om[4]
#define	oZ	om[5]

//#define	DISTANCE	((pX-oX)*(pX-oX)+(pY-oY)*(pY-oY)+(pZ-oZ)*(pZ-oZ))

namespace ProjetEPFL
{

class Camera : public PCEntree
{
	public :

		//	Constructeur
		Camera () ;
		
		//	Méthodes
		void init () ;
		void miseAJour () ;
		void regarde () const ;

		//	Destructeur
		~Camera () ;
	
	protected :

		double sensibiliteMouvement ;
		double sensibiliteMolette ;
		//SDL_Cursor* cur[NB_CURSEURS] ;
		Vecteur om ;				/* Contient :
								Position de l'oeil + 3angles, 2 pour la direction, 1 pour la "rotation".*/
};

}

#endif	// PROJETEPFL_CAMERA_H_INCLUDED
