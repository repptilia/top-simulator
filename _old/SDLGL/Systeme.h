#ifndef PROJETEPFL_SYSTEME_H_INCLUDED
#define PROJETEPFL_SYSTEME_H_INCLUDED

#define	NB_TEXTURES		1

#include "Camera.h"
#include "ConeSimple.h"
#include "Chinoise.h"
#include "IntegrateurEuler.h"

namespace ProjetEPFL
{

class Systeme : public std::vector<Toupie*>, public Dessinable, public Camera
{
	public :

		//	Constructeur
		Systeme () ;
		
		//	Méthodes
		void affiche ( std::ostream &out ) const ;
		void ajoute ( const Toupie& toupie ) ;
		void supprime () ;
		void dessine ( SortieDessin sortie = ECRAN ) const ;
		void dessineEnvironnement () const ;
		void GestionEvenements () ;
		
		int active () ;

		//	Destructeur
		~Systeme () ;

	protected :

		//	Méthodes
		Systeme ( const Systeme &systeme ) ;
		Systeme operator= ( const Systeme &systeme ) ;

	
		double g ;				//	Faire varier la gravité ?
		Integrateur* integrateur ;	//	Intégrateur (pointeur pour en changer de façon polymorphique
		Vecteur om ;			//	Position + inclinaison du repère
		double dt ;				//	Pas de temps d'intégration + de calcul
		double t ;				//	Temps absolu
		GLuint texture[NB_TEXTURES] ;	//	Textures
};

//	Opérateurs externes
std::ostream &operator<< ( std::ostream &out, const Systeme &systeme ) ;

}
#endif //PROJETEPFL_SYSTEME_H_INCLUDED
