#include "Vecteur.h"

namespace ProjetEPFL
{

//	Constructeurs

Vecteur::Vecteur () : std::vector<double>()
{
}

Vecteur::Vecteur ( double v1, double v2 , double v3, double v4, double v5 )
{
	push_back(v1) ;
	push_back(v2) ;
	push_back(v3) ;
	push_back(v4) ;
	push_back(v5) ;
}	

Vecteur::Vecteur ( unsigned int taille, double elementParDefaut ) : std::vector<double> ( taille, elementParDefaut )
{
}

//	Méthodes

void Vecteur::affiche ( std::ostream &out ) const //	Prend un flux en paramètre, ce qui permet de rediriger l'affichage facilement vers telle ou telle sortie.
{
	if ( size() > 0 )
	{
		std::vector<double>::const_iterator i(begin()) ;
		std::vector<double>::const_iterator j ( end() - 1 ) ;
		
		out << "(" ;
	
		for ( ; i != j ; ++i )
			out << *i << ", " ;
	
		out << *i << ")" ;
	}
}

double Vecteur::norme () const
{
	return sqrt(normeCarree()) ;
}

double Vecteur::normeCarree () const 
{
	return this->operator*(*this) ;
}


//	Opérateurs internes

Vecteur& Vecteur::operator+= ( const Vecteur &vecteur )
{
	if ( size() != vecteur.size() ) throw std::length_error("Vecteur& Vecteur::operator+=(const Vecteur&) : Dim mismatch") ;

	for ( std::vector<double>::iterator i(begin()) ; i != end() ; ++i )
		*i += *(vecteur.begin() + (i - begin())) ;
	
	return *this ;
}

Vecteur Vecteur::operator+ ( const Vecteur &vecteur ) const
{
	return Vecteur(*this) += vecteur ;
}

Vecteur& Vecteur::operator-= ( const Vecteur &vecteur )
{
	if ( size() != vecteur.size() ) throw std::length_error("Vecteur& Vecteur::operator-=(const Vecteur&) : Dim mismatch") ;

	for ( std::vector<double>::iterator i(begin()) ; i != end() ; ++i )
		*i -= *(vecteur.begin() + (i - begin() ) ) ;
	
	return *this ;
}

Vecteur Vecteur::operator- ( const Vecteur &vecteur ) const
{
	return Vecteur(*this) -= vecteur ;
}

Vecteur& Vecteur::operator^= ( const Vecteur &vecteur )
{
	if ( size() != 3 || vecteur.size() != 3 ) throw std::length_error("Vecteur& Vecteur::operator^=(const Vecteur&) : Dim != 3") ;

	double temp1, temp2 ;	//	Variables créées pour éviter d'écraser le tableau pendant sa modification.

				temp1 = *(begin() + 1) * *(vecteur.begin() + 2) - *(begin() + 2) * *(vecteur.begin() + 1) ;
				temp2 = *(begin() + 2) * *(vecteur.begin() + 0) - *(begin() + 0) * *(vecteur.begin() + 2) ;
	*(begin() + 2) = *(begin() + 0) * *(vecteur.begin() + 1) - *(begin() + 1) * *(vecteur.begin() + 0) ;
	*(begin() + 1) = temp2 ;
	*(begin() + 0) = temp1 ;

	return *this ;
}

Vecteur Vecteur::operator^ ( const Vecteur &vecteur ) const
{
	return Vecteur(*this) ^= vecteur ;
}

double Vecteur::operator* ( const Vecteur &vecteur ) const
{	
	if ( size() != vecteur.size() ) throw std::length_error("Vecteur& Vecteur::operator-=(const Vecteur&) : Dim mismatch") ;

	double produitScalaire(0.0) ;

	for ( std::vector<double>::const_iterator i(begin()) ; i != end() ; ++i )
		produitScalaire += *i * *(vecteur.begin() + (i - begin())) ;
	
	return produitScalaire ;
}

Vecteur& Vecteur::operator*= ( double scalaire )
{
	for ( std::vector<double>::iterator i(begin()) ; i != end() ; ++i )
		*i *= scalaire ;
		
	return *this ;
}

Vecteur Vecteur::operator* ( double scalaire ) const
{
	return Vecteur(*this) *= scalaire ;
}

Vecteur Vecteur::operator! () const
{		
	return Vecteur(*this) *= -1 ;
}


//	Destructeur

Vecteur::~Vecteur ()
{
}


//	Opérateurs externes

std::ostream& operator<< ( std::ostream &out, Vecteur vecteur )
{
	vecteur.affiche(out) ;
	return out ;
}

Vecteur operator* ( double scalaire, const Vecteur &vecteur )
{
	return vecteur * scalaire ;
}

}
