#ifndef PROJEPFL_ERREUR_H_INCLUDED
#define PROJEPFL_ERREUR_H_INCLUDED

#include <iostream>
#include <string>
#include <stdexcept>

/*
Classe de gestion des Erreurs
Conception optimisée pour simplifier le code.

Utilisation du constructeur surchargé recommandée (avec un throw):
throw Erreur([message],[codeErreur]) ;
*/

#define	AUCUNE_ERREUR			0
#define	LIMITE_VECTEUR_DEPASSEE		1
#define	TAILLE_VECTEURS_DIFFERENTE	2
#define	TAILLE_VECTEUR_DIFF_3		3
#define	DETERMINANT_NUL			4

namespace ProjetEPFL
{

class Erreur
{
	public :
	
	//	Constructeurs
		Erreur () ;
		Erreur ( std::string message, int repere ) ;
		
	//	Fonctions internes
		void affiche ( std::ostream &out ) ;
	
	//	Attributs
		std::string m_message ;
		int m_repere ;
};

std::ostream& operator<< ( std::ostream &out, Erreur &erreur ) ;

}

#endif // PROJEPFL_VECTEUR_H_INCLUDED
