#include "Matrice33.h"

namespace ProjetEPFL
{

//	Constructeurs

Matrice33::Matrice33 ()
{
	tab[3][3]={0};
}

Matrice33::Matrice33 ( double a )
{
	for(unsigned int i=0;i<3;i++){
		for(unsigned int j=0;j<3;j++){
			tab[i][j]=a;
		}
	}
}

Matrice33::Matrice33 ( double a, double b, double c )
{
	tab[3][3]={0};
	tab[0][0] = a ;
	tab[1][1] = b ;
	tab[2][2] = c ;
}

Matrice33::Matrice33 ( double a, double b, double c, double d, double e, double f, double g, double h, double i )
{
	tab[0][0] = a ;
	tab[0][1] = b ;
	tab[0][2] = c ;
	tab[1][0] = d ;
	tab[1][1] = e ;
	tab[1][2] = f ;
	tab[2][0] = g ;
	tab[2][1] = h ;
	tab[2][2] = i ;
}


//	Méthodes

Matrice33 Matrice33::diagonale () const
{
	Matrice33 autreMatrice ;
	
	for ( unsigned int i(0) ; i < 3 ; ++i )
		autreMatrice.tab[i][i] = tab[i][i] ;
	
	return autreMatrice ;	
}

Matrice33 Matrice33::transp () const
{	
	Matrice33 autreMatrice ;
	for (int i(0); i<3 ; ++i)
	{
		for ( int j(0); j<3; ++j)
			autreMatrice.tab[i][j] = tab[j][i];
	}
	return autreMatrice;
}

double Matrice33::det () const
{
	double determinant (
		tab[0][0]*tab[1][1]*tab[2][2] +
		tab[0][1]*tab[1][2]*tab[2][0] +
		tab[0][2]*tab[1][0]*tab[2][1] -
		tab[0][0]*tab[1][2]*tab[2][1] -
		tab[0][1]*tab[1][0]*tab[2][2] -
		tab[0][2]*tab[1][1]*tab[2][0] ) ;
		
	if ( abs(determinant) < PRECISION_MAXIMUM_DOUBLE ) return 0 ;
	
	return determinant ;
}

Matrice33 Matrice33::inv () const
{
	double determinant = det () ;
	
	if ( determinant == 0.0 ) throw Erreur("Matrice33 Matrice33::inv() const", DETERMINANT_NUL) ;

	return Matrice33 ( 	 ( tab[1][1]*tab[2][2] - tab[1][2]*tab[2][1] ) / determinant ,
				 ( tab[0][2]*tab[2][1] - tab[0][1]*tab[2][2] ) / determinant ,
				 ( tab[0][1]*tab[1][2] - tab[0][2]*tab[1][1] ) / determinant ,
				 ( tab[1][2]*tab[2][0] - tab[1][0]*tab[2][2] ) / determinant ,
				 ( tab[0][0]*tab[2][2] - tab[0][2]*tab[2][0] ) / determinant ,
				 ( tab[0][2]*tab[1][0] - tab[0][0]*tab[1][2] ) / determinant ,
				 ( tab[1][0]*tab[2][1] - tab[1][1]*tab[2][0] ) / determinant ,
				 ( tab[0][1]*tab[2][0] - tab[0][0]*tab[2][1] ) / determinant ,
				 ( tab[0][0]*tab[1][1] - tab[0][1]*tab[1][0] ) / determinant ) ;
}

void Matrice33::affiche ( std::ostream& out ) const
{
	for ( unsigned int i(0) ; i < 3 ; ++i )
		out << "[" << tab[i][0] << " " << tab[i][1] << " " << tab[i][2] << "]" << std::endl ;
}

void Matrice33::round ()
{
	for ( unsigned int i(0) ; i < 3 ; ++i )
	{
		for ( unsigned int j(0) ; j < 3 ; ++j )
			tab[i][j] *= ( abs(tab[i][j]) > PRECISION_MAXIMUM_DOUBLE ) ;
	}
}

//	Opérateurs internes

Matrice33& Matrice33::operator+= ( Matrice33 autreMatrice )
{	
	for (int i(0) ; i < 3 ; ++i )
	{
		for ( int j(0) ; j < 3 ; ++j )
			tab[i][j] += autreMatrice.tab[i][j] ;
	}
	return *this ;
}
Matrice33 Matrice33::operator+ ( Matrice33 autreMatrice ) const
{
	return Matrice33(*this) += autreMatrice ;
}

Matrice33 Matrice33::operator- ( Matrice33 autreMatrice ) const
{	
	return *this + autreMatrice * -1.0 ;
}

Matrice33& Matrice33::operator*= ( double scal )
{	
	for (int i(0); i<3 ; ++i)
	{
		for ( int j(0); j<3; ++j )
			tab[i][j] *= scal ;
	}
	return *this;
}

Matrice33 Matrice33::operator* ( double scal ) const
{	
	return Matrice33(*this) *= scal ;
}

Matrice33& Matrice33::operator*= ( Matrice33 autreMatrice )
{
	int m1, m2, m3 ; // variables crées pour éviter d'écraser le tableau pendant sa modification
	
	for ( unsigned int i(0) ; i < 3 ; ++i )
	{
		for ( unsigned int j(0) ; j < 3 ; ++j )
		{
			m1 = tab[i][0] * autreMatrice.tab[0][j] ;
			m2 = tab[i][1] * autreMatrice.tab[1][j] ;
			m3 = tab[i][2] * autreMatrice.tab[2][j] ;

			tab[i][j] = m1 + m2 + m3 ;
		}
	}
	return *this ;
}

Matrice33 Matrice33::operator* ( Matrice33 autreMatrice ) const
{
	return Matrice33(*this) *= autreMatrice ;
}

Vecteur Matrice33::operator* ( Vecteur autreVecteur ) const
{	
	return Vecteur (
			tab[0][0] * autreVecteur[0] + tab[0][1] * autreVecteur[1] + tab[0][2] * autreVecteur[2],
			tab[1][0] * autreVecteur[0] + tab[1][1] * autreVecteur[1] + tab[1][2] * autreVecteur[2],
			tab[2][0] * autreVecteur[0] + tab[2][1] * autreVecteur[1] + tab[2][2] * autreVecteur[2]) ;
}

Matrice33 operator* ( double scalaire, Matrice33 matrice33 )
{
	return matrice33 * scalaire ;
}

double& Matrice33::operator() ( unsigned int i, unsigned int j )
{
	if ( i > 2 || j > 2 ) throw Erreur("double& operator[]()", 48) ;

	return tab[i][j] ;
}

//	Destructeurs

Matrice33::~Matrice33 ()
{
}

//	Opérateurs externes

std::ostream& operator<< ( std::ostream& out, Matrice33 matrice33 )
{
	matrice33.affiche(out) ;
	
	return out ;
}

}
	
