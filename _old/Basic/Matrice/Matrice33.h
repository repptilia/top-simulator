#ifndef PROJETEPFL_MATRICE33_H_INCLUDED
#define PROJETEPFL_MATRICE33_H_INCLUDED

#include <iostream>
#include "../Vecteur.h"

#define	PRECISION_MAXIMUM_DOUBLE	1e-14

namespace ProjetEPFL
{

class Matrice33
{
	public :
	
		//	Constructeurs	
		Matrice33 () ;
		Matrice33 ( double a ) ;
		Matrice33 ( double a, double b, double c ) ;
		Matrice33 ( double a, double b, double c, double d, double e, double f, double g, double h, double i ) ;
		
		//	Méthodes
		Matrice33 diagonale () const ;
		Matrice33 transp () const ;
		double det() const ;
		Matrice33 inv () const ;
		void affiche ( std::ostream& out ) const ;
		void round () ;
		
		//	Opérateurs internes
		Matrice33& operator+= ( Matrice33 autreMatrice ) ;
		Matrice33 operator+ ( Matrice33 autreMatrice ) const ;
		Matrice33 operator- ( Matrice33 autreMatrice ) const ;
		Matrice33& operator*= (double scal ) ;
		Matrice33 operator* ( double scal ) const ;
		Matrice33& operator*= ( Matrice33 autreMatrice ) ;
		Matrice33 operator* ( Matrice33 autreMatrice ) const ;
		Vecteur operator* ( Vecteur autreVecteur ) const ;
		double& operator() ( unsigned int i, unsigned int j ) ;

		//	Destructeurs
		~Matrice33 () ;
		
	private :
	
		double tab[3][3] ;
};

//	Opérateurs externes
Matrice33 operator* ( double scalaire, Matrice33 matrice33 ) ;
std::ostream& operator<< ( std::ostream& out, Matrice33 matrice33 ) ;

}

#endif // PROJETEPFL_MATRICE33_H_INCLUDED
