#ifndef PROJETEPFL_DESSINABLE_H_INCLUDED
#define PROJETEPFL_DESSINABLE_H_INCLUDED

#define	_2PI	6.283185307			//Utilisé pour éviter de faire des calculs dans Integrateur
#define	_g	9.81

#include "Vecteur.h"
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

namespace ProjetEPFL
{

enum SortieDessin { CONSOLE, CONSOLE_cerr, CONSOLE_clog, ECRAN } ;

class Dessinable
{
	public :
	
		// Méthode
		virtual void dessine ( SortieDessin sortie = ECRAN ) const = 0 ;
};

}
#endif //PROJETEPFL_DESSINABLE_H_INCLUDED
