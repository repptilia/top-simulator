#ifndef PROJETEPFL_VECTEUR_H_INCLUDED
#define PROJETEPFL_VECTEUR_H_INCLUDED

#include <cmath>
#include <vector>
#include <iterator>
#include "Erreur.h"

namespace ProjetEPFL
{

class Vecteur : public std::vector<double>
{
	public :
		//	Constructeurs
		Vecteur () ;
		Vecteur ( double v1, double v2, double v3, double v4 = 0.0, double v5 = 0.0 ) ;
		Vecteur ( unsigned int taille, double elementParDefaut = 0.0 ) ;
		
		//	Méthodes
		void affiche ( std::ostream &out ) const ;
		double norme () const ;
		double normeCarree () const ;

		//	Opérateurs internes
		Vecteur& operator+= ( const Vecteur &vecteur ) ;
		Vecteur operator+ ( const Vecteur &vecteur ) const ;
		Vecteur& operator-= ( const Vecteur &vecteur ) ;
		Vecteur operator- ( const Vecteur &vecteur ) const ;
		Vecteur& operator^= ( const Vecteur &vecteur ) ;
		Vecteur operator^ ( const Vecteur &vecteur ) const ;
		Vecteur& operator*= ( double scalaire ) ;
		Vecteur operator* ( double scalaire ) const ;
		double operator* ( const Vecteur &vecteur ) const ;
		Vecteur operator! () const ;

		//	Destructeur
		~Vecteur () ;
};

//	Opérateurs externes
std::ostream& operator<< ( std::ostream &out, Vecteur vecteur ) ;
Vecteur operator*( double scalaire, const Vecteur &vecteur ) ;

}

#endif // PROJETEPFL_VECTEUR_H_INCLUDED
