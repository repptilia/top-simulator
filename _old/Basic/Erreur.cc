#include "Erreur.h"

namespace ProjetEPFL
{


//	Constructeurs
Erreur::Erreur ()
{
	m_message = "" ;
	m_repere = 0 ;
}

Erreur::Erreur ( std::string message, int repere )
{
	m_message = message ;
	m_repere = repere ;
}


//	Fonctions internes

void Erreur::affiche ( std::ostream &out )
{
	if ( m_repere )
		out << "Erreur " << m_repere << " : " << m_message ;
}



//	Fonctions externes

std::ostream& operator<< ( std::ostream &out, Erreur &erreur )
{
	erreur.affiche(out) ;
	return out ;
}

}
