#include "Integrateur.h"

namespace ProjetEPFL
{

//	Constructeur

Integrateur::Integrateur () : dt(1e-3)
{
}

Integrateur::Integrateur ( double dt2 ) : dt(dt2)
{
}


//	Méthodes
double Integrateur::t() const
{
	return dt ;
}


//	Opérateurs internes

double& Integrateur::operator= ( double t )
{
	dt = t ;
	
	return dt ;
}

}
