#include "IntegrateurRungeKutta4.h"

namespace ProjetEPFL
{

IntegrateurRungeKutta4::IntegrateurRungeKutta4 () : Integrateur()
{
}

void IntegrateurRungeKutta4::integre ( Toupie* toupie, double t ) const
{
	Vecteur k[4], k_[4] ;
	
	k[0] 	= toupie->domega () ;
	k_[0] = toupie->equationEvolution (t) ;
	k[1] 	= k[0] + dt/2.0*k_[0] ;
	k_[1] = toupie->equationEvolution (t,dt/2.0*k[0], dt/2.0*k_[0]) ;
	k[2]	= k[0] + dt/2.0*k_[1] ;
	k_[2] = toupie->equationEvolution (t,dt/2.0*k[1], dt/2.0*k_[1]) ;	
	k[3]	= k[0] + dt/2.0*k_[2] ;
	k_[3] = toupie->equationEvolution (t,dt/2.0*k[2], dt/2.0*k_[2]) ;
	
	toupie->omega() += dt/6.0*(k[0]+2*k[1]+2*k[2]+k[3]) ;
	toupie->domega() += dt/6.0*(k_[0]+2*k_[1]+2*k_[2]+k_[3]) ;
}

Integrateur* IntegrateurRungeKutta4::copie () const
{
	return new IntegrateurRungeKutta4(*this) ;
}

}
