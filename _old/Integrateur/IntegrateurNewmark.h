#ifndef PROJETEPFL_INTEGRATEURNEWMARK_H_INCLUDED
#define PROJETEPFL_INTEGRATEURNEWMARK_H_INCLUDED

#include "Integrateur.h"

namespace ProjetEPFL
{

class IntegrateurNewmark : public Integrateur
{
	public :
	
		IntegrateurNewmark () ;
		IntegrateurNewmark ( double dt, double epsilon22 ) ;
	
		//	Méthodes
		virtual void integre ( Toupie* toupie, double t = 0 ) const ;
		virtual Integrateur* copie () const ;
		
	private :
		
		double epsilon2 ;

};

}

#endif	//PROJETEPFL_INTEGRATEURNEWMARK_H_INCLUDED
