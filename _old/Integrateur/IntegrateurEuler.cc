#include "IntegrateurEuler.h"

namespace ProjetEPFL
{

IntegrateurEuler::IntegrateurEuler () : Integrateur()
{
}

IntegrateurEuler::IntegrateurEuler ( double dt ) : Integrateur(dt)
{
}

void IntegrateurEuler::integre ( Toupie* toupie, double t ) const
{
	toupie->domega() += dt * toupie->equationEvolution(t) ;
	toupie->omega() += dt * toupie->domega() ;
	
	for ( unsigned int i(0) ; i < 3 ; ++i )
	{
		while ( toupie->omega()[i] > _2PI )
			toupie->omega()[i] -= _2PI ;
		while ( toupie->omega()[i] < 0 )
			toupie->omega()[i] += _2PI ;
	}
	
}

Integrateur* IntegrateurEuler::copie () const
{
	return new IntegrateurEuler(*this) ;
}

}

