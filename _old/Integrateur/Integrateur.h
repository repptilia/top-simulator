#ifndef PROJETEPFL_INTEGRATEUR_H_INCLUDED
#define PROJETEPFL_INTEGRATEUR_H_INCLUDED

#include "Chinoise.h"
#include "ConeSimple.h"

namespace ProjetEPFL
{

class Integrateur
{
	public :

		//	Constructeur
		Integrateur () ;
		Integrateur ( double dt ) ;
		
		//	Méthodes
		double t() const ;
		virtual void integre ( Toupie* toupie, double t = 0 ) const = 0 ;
		virtual Integrateur* copie () const = 0 ;
		
		//	Opérateurs internes
		double& operator= ( double t ) ;

	protected :

		double dt ;
};

}

#endif // PROJETEPFL_INTEGRATEUR_H_INCLUDED

