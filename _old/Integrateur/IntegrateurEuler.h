#ifndef PROJETEPFL_INTEGRATEUREULER_H_INCLUDED
#define PROJETEPFL_INTEGRATEUREULER_H_INCLUDED

#include "Integrateur.h"

namespace ProjetEPFL
{

class IntegrateurEuler : public Integrateur
{
	public :
	
		//	Constructeurs
		IntegrateurEuler () ;
		IntegrateurEuler ( double dt ) ;
	
		//	Méthodes
		virtual void integre ( Toupie* toupie, double t = 0 ) const ;
		virtual Integrateur* copie () const ;

	private :

};

}

#endif // PROJETEPFL_INTEGRATEUREULER_H_INCLUDED

