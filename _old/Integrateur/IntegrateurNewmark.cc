#include "IntegrateurNewmark.h"

namespace ProjetEPFL
{

IntegrateurNewmark::IntegrateurNewmark () : Integrateur(), epsilon2(1e-4)
{
}

IntegrateurNewmark::IntegrateurNewmark ( double dt, double epsilon22 ) : Integrateur(dt), epsilon2(epsilon22)
{
}

void IntegrateurNewmark::integre ( Toupie* toupie, double t ) const
{
	Vecteur khi,xi, khip(toupie->equationEvolution(t)) ;
	
	do
	{
		xi = toupie->omega() ;
		khi = toupie->equationEvolution(t) ;
		
		toupie->omega() += dt*toupie->domega() + dt*dt/3.0*(0.5*khi+khip) ;
		toupie->domega() += dt*(khi+khip)*0.5 ;
	}
	while ( Vecteur(toupie->omega() - xi).normeCarree() > epsilon2 ) ;
}

Integrateur* IntegrateurNewmark::copie () const
{
	return new IntegrateurNewmark(*this) ;
}

}
