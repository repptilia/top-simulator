#ifndef PROJETEPFL_INTEGRATEURRUNGEKUTTA4_H_INCLUDED
#define PROJETEPFL_INTEGRATEURRUNGEKUTTA4_H_INCLUDED

#include "Integrateur.h"

namespace ProjetEPFL
{

class IntegrateurRungeKutta4 : public Integrateur
{
	public :
		
		//	Constructeurs
		IntegrateurRungeKutta4 () ;
		
		//	Méthodes
		virtual void integre ( Toupie* toupie, double t = 0 ) const ;
		virtual Integrateur* copie () const ;
			
	protected :

		double h ;
		double r ;
};

}

#endif	//INTEGRATEURRUNGEKUTTA4
