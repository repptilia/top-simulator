#include "Vecteur.h"

using namespace ProjetEPFL ;

int main ( int argc, char** argv )
{
try{
	Vecteur v1(10,0) ;
	
	for ( unsigned int i(0) ; i < v1.size() ; ++i )
		v1[i] = i ;
	
	std::cout << v1 << std::endl ;
	}
	catch ( Erreur err )
	{
		std::cout << "|||->Erreurs<-|||" << std::endl << err << std::endl ;
	}
	catch ( std::domain_error err )
	{
		std::cout << err.what() << std::endl ;
	}
	
	return 0 ;
}
