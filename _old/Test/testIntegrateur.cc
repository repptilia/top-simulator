#include "IntegrateurEuler.h"
#include "IntegrateurNewmark.h"

using namespace ProjetEPFL ;

int main ( int argc, char** argv )
{
	Chinoise* autreToupie = new Chinoise ;
	IntegrateurEuler in(1e-2) ;

	double t(0) ;

	for ( unsigned int i(0) ; i < 20 ; i ++ )
	{
		std::cout << "t=" << t << std::endl ;
		autreToupie->dessine() ;
		std::cout << std::endl ;
		
		in.integre(autreToupie) ;
		t+=in.t() ;
	}

	return 0 ;
}
