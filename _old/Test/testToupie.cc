#include "ConeSimple.h"
#include "Chinoise.h"

using namespace ProjetEPFL ;

int main ( int argc, char** argv )
{
	ConeSimple toupie ;
	double MV(0.1), H(0), R(0.5) ;
	
	for ( int i(0) ; i < 10 ; i ++ )
	{
		toupie = ConeSimple ( MV, H, R ) ;
		{
			H += 1 ;
		}
		
		std::cout << toupie << std::endl ;
	}
	return 0 ;
}
