#include "Matrice33.h"

using namespace ProjetEPFL ;
using namespace std ;

int main()
{
	try
	{
		Matrice33 I;
		Matrice33 diag(1., 2., 3.);
		Matrice33 mat(1.1, 2.4,5.4,1.2,3.5,4.7,8.0,1.2,3.1);
		Vecteur v(3) ;
			v[0] = 5.5 ; v[1] = 6.6 ; v[2] = 7.7 ;
		cout << mat.inv() << endl ;
		cout << mat+mat << endl ;
		cout << mat-mat << endl;
		cout << diag-I << endl;
		cout << 4.4*diag << endl;
		cout << 2*mat << endl;
		cout << mat*v << endl;
		cout << mat*mat << endl;
		Matrice33 mat2(mat*mat.inv()) ;
		mat2.round() ;
		cout << mat2 << endl ;
		cout << mat.transp() << endl;
	}
	catch ( Erreur erreur )
	{
		std::cerr << erreur << std::endl ;
	}
	
	return 0;
}

