#include "ConeSimple.h"

namespace ProjetEPFL
{

//	Constructeurs

ConeSimple::ConeSimple () : Toupie(), h(1.5), r(0.5)
{
	mV = 0.1 ;
}

ConeSimple::ConeSimple ( double MV, double H, double R )
: Toupie(), h(H), r(R)
{
	mV = MV ;
}


//	Méthodes

Vecteur ConeSimple::equationEvolution ( double t ) const
{
	return Vecteur ( ddpsiC, ddthetaC, ddphiC ) ;
}

Vecteur ConeSimple::equationEvolution ( double t, Vecteur om, Vecteur dom ) const
{
	return Vecteur ( ddpsiC, ddthetaC, ddphiC ) ;
}

void ConeSimple::affiche ( std::ostream &out ) const
{
	out << "Cône Simple" << std::endl << 
		 ">Rayon     : " << r << std::endl << 
		 ">Hauteur   : " << h << std::endl << 
		 ">Masse Vol : " << mV << std::endl ;
	
	Toupie::affiche(out) ;
}

void ConeSimple::dessine ( SortieDessin sortie ) const
{
	GLUquadric* quadrique = gluNewQuadric() ;

	switch ( sortie )
	{
		case ECRAN :
			glPushMatrix() ;
			std::cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << "theta\ndeg:"<<deg(theta)<<"\nrad:"<<theta << std::endl ;
			glRotated(deg(psi), 0.0,0.0,1.0) ;	// psi autour de z
			glRotated(deg(theta),0.0, 1.0,0.0) ;
			glRotated(deg(phi), 0.0,1.0,0.0) ;
			gluQuadricDrawStyle(quadrique,GLU_LINE);
			gluCylinder(quadrique,0.0,r,h,20,1);

			gluDeleteQuadric(quadrique);
			glPopMatrix() ;
			break ;

		case CONSOLE :
			affiche(std::cout) ;
			break ;
		
		default :
			break ;
	}
}

Toupie* ConeSimple::copie () const
{
	return new ConeSimple(*this) ;
}


//	Opérateurs externes

std::ostream &operator<< ( std::ostream &out, ConeSimple &cone )
{
	cone.affiche(out) ;
	
	return out ;
}


double deg ( double angle )
{
	return angle * 360.0 / 2.0 / M_PI ;
}

double rad ( double angle )
{
	return angle * 2.0 * M_PI / 360.0 ;
}

}
