#ifndef PROJETEPFL_CHINOISE_H_INCLUDED
#define PROJETEPFL_CHINOISE_H_INCLUDED

#define	mCh	(M_PI*mV*((4.0/3.0)*r*r*r-h*h*(r-h/3.0)))
#define alphaCh	(3.0*h*h/(4.0*r*(r+h)))
#define	I3Ch	(M_PI*mV*(2.0*r-h)*(2.0*r-h)*(2.0*r-h)*(2.0*r*r+3.0*h*r+3.0*h*h)/30.0)
#define	I1Ch	(0.5*I3Ch+(M_PI*mV*(2.0*r-h)*(2.0*r-h)*(r*r*r+h*r*r-3.0*h*h*r+3.0*h*h*h))/15.0-mCh*r*r*alphaCh*alphaCh)
#define f1Ch	(dphi+dpsi*cos(theta))
#define f3Ch	(I1Ch*I3Ch+mCh*r*r*I1Ch*sin(theta)*sin(theta)+mCh*r*r*I3Ch*(alphaCh-cos(theta))*(alphaCh-cos(theta)))
#define f2Ch	(dtheta*f1Ch*I3Ch*(I3Ch+mCh*r*r*(1.0-alphaCh*cos(theta)))/(sin(theta)*f3Ch)-2.0*dpsi*dtheta*(cos(theta)/sin(theta)))
#define ddpsiCh	(f2Ch)
#define ddthetaCh	(sin(theta)*(dpsi*dpsi*(-mCh*r*r*(alphaCh-cos(theta))*(1.0-alphaCh*cos(theta))+I1Ch*cos(theta))+f1Ch*dpsi*(mCh*r*r*(alphaCh*cos(theta)-1.0)-I3Ch)-mCh*r*r*dtheta*dtheta*alphaCh-mCh*r*alphaCh*_g)/(I1Ch+mCh*r*r*((alphaCh-cos(theta))*(alphaCh-cos(theta))+sin(theta)*sin(theta))))
#define ddphiCh	(dpsi*dtheta*sin(theta)-cos(theta)*f2Ch-f1Ch*dtheta*sin(theta)*(mCh*r*r*(I3Ch*(alphaCh-cos(theta))+I1Ch*cos(theta)))/f3Ch)
#define ddchiCh	(r*(dtheta*sin(psi)-dphi*cos(psi)*sin(theta)))
#define ddxsiCh	(-r*(dtheta*cos(psi)-dphi*sin(psi)*sin(theta)))

#include "Toupie.h"

namespace ProjetEPFL
{

class Chinoise : public Toupie
{
	public :
	
		//	Constructeurs
		Chinoise() ;
		Chinoise( double MV, double H, double R) ;
		
		//	Méthodes
		Vecteur equationEvolution ( double t ) const ;
		Vecteur equationEvolution ( double t, Vecteur om, Vecteur dom ) const ;
		void affiche ( std::ostream &out ) const ;
		virtual void dessine ( SortieDessin sortie = ECRAN ) const ;
		virtual Toupie* copie () const ;
	
	protected :
	
		double h ;
		double r ;
	
};

//	Opérateurs externes
std::ostream &operator<< ( std::ostream &out, Chinoise &chine ) ;

}

#endif //PROJETEPFL_CHINOISE_H_INCLUDED
