#include "Toupie.h"

namespace ProjetEPFL
{

//	Constructeur

Toupie::Toupie() : om(0,M_PI/6.0,0), dom(0,0,60)
{
}

Toupie::Toupie ( Vecteur v1, Vecteur v2 )	: om(v1), dom(v2)
{
}


//	Méthodes

Vecteur Toupie::equationEvolution ( double t ) const
{
	return Vecteur() ;
}

Vecteur Toupie::equationEvolution ( double t, Vecteur om, Vecteur dom ) const
{
	return Vecteur() ;
}

void Toupie::affiche ( std::ostream &out ) const
{
	out << ">Omega  : " << om << std::endl <<
		 ">Omega' : "<< dom ;
}

Vecteur& Toupie::omega ()
{
	return om ;
}

Vecteur& Toupie::domega ()
{
	return dom ;
}

void Toupie::dessine ( SortieDessin sortie ) const
{
	switch ( sortie )
	{
		case CONSOLE :
			std::cout << "Toupie (Objet Abstrait)" << std::endl ;
			affiche( std::cout ) ;

			break ;

		case ECRAN :
			glPushMatrix () ;
			 
			// effectue la rotation
			glRotated(theta, 0.0, 1.0, 0.0) ;        //(angle, axe)
			glRotated(phi,   0.0, 0.0, 1.0) ;
			 
			// dessine les 6 faces du cube
			glBegin(GL_QUADS);
			glColor4d(0.0, 0.0, 1.0, 1.0); // bleu
			glVertex3d( 0.5, 0.5, 0.5); glVertex3d(-0.5, 0.5, 0.5);
			glVertex3d(-0.5,-0.5, 0.5); glVertex3d( 0.5,-0.5, 0.5);
			 
			glColor4d(0.0, 1.0, 1.0, 1.0); // turquoise (cyan=bleu+vert)
			glVertex3d(-0.5,-0.5,-0.5); glVertex3d(-0.5, 0.5,-0.5);
			glVertex3d( 0.5, 0.5,-0.5); glVertex3d( 0.5,-0.5,-0.5);
			 
			glColor4d(1.0, 0.0, 1.0, 1.0); // violet (magenta=rouge+bleu)
			glVertex3d( 0.5, 0.5, 0.5); glVertex3d( 0.5, 0.5,-0.5);
			glVertex3d(-0.5, 0.5,-0.5); glVertex3d(-0.5, 0.5, 0.5);
			 
			glColor4d(1.0, 0.0, 0.0, 1.0); // rouge
			glVertex3d(-0.5,-0.5,-0.5); glVertex3d( 0.5,-0.5,-0.5);
			glVertex3d( 0.5,-0.5, 0.5); glVertex3d(-0.5,-0.5, 0.5);
			 
			glColor4d(1.0, 1.0, 0.0, 1.0); // jaune (=rouge+vert)
			glVertex3d( 0.5, 0.5, 0.5); glVertex3d( 0.5,-0.5, 0.5);
			glVertex3d( 0.5,-0.5,-0.5); glVertex3d( 0.5, 0.5,-0.5);
			 
			glColor4d(1.0, 1.0, 1.0, 1.0); // blanc (=rouge+vert+bleu)
			glVertex3d(-0.5,-0.5,-0.5); glVertex3d(-0.5,-0.5, 0.5);
			glVertex3d(-0.5, 0.5, 0.5); glVertex3d(-0.5, 0.5,-0.5);
			glEnd();
			
			glPopMatrix() ;
			break ;

		default :
			break ;
	}
}

Toupie* Toupie::copie () const
{
	return new Toupie(*this) ;
}

//	Opérateur externe

std::ostream &operator<< ( std::ostream &out, Toupie &toupie )
{
	toupie.affiche(out) ;

	return out;
}

}
