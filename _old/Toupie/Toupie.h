#ifndef PROJETEPFL_TOUPIE_H_INCLUDED
#define PROJETEPFL_TOUPIE_H_INCLUDED

#define	psi		om[0]
#define	theta		om[1]
#define	phi		om[2]
#define	chi		om[3]
#define	xsi		om[4]

#define	dpsi		dom[0]
#define	dtheta		dom[1]
#define	dphi		dom[2]
#define	dchi		dom[3]
#define	dxsi		dom[4]


#include <fstream>
#include "Dessinable.h"

namespace ProjetEPFL
{

class Toupie : public Dessinable
{
	public :
		
		//	Constructeurs
		Toupie () ;
		Toupie ( Vecteur v1, Vecteur v2 ) ;
		
		//	Méthodes
		virtual Vecteur equationEvolution ( double t ) const ;
		virtual Vecteur equationEvolution ( double t, Vecteur om, Vecteur dom ) const ;
		virtual void affiche ( std::ostream &out ) const ;
		Vecteur& omega () ;
		Vecteur& domega () ;
		virtual void dessine ( SortieDessin sortie = ECRAN ) const ;
		virtual Toupie* copie () const ;

	protected :
	
		Vecteur om ;
		Vecteur dom ;
		double mV ;
};

//	Opérateurs externes
std::ostream &operator<< ( std::ostream &out, Toupie &toupie ) ;
std::ofstream& operator<< ( std::ofstream &out, Toupie &cone ) ;

}

#endif //PROJETEPFL_TOUPIE_H_INCLUDED
