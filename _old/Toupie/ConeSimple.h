#ifndef PROJETEPFL_CONESIMPLE_H_INCLUDED
#define PROJETEPFL_CONESIMPLE_H_INCLUDED

#define	mC		( r*r * h * M_PI * mV/3.0 )
#define	I1C		( ( 3.0 * mC/20.0 ) * ( r*r + h*h/4.0 ) )
#define	I3C		( mC * 0.3 * r*r )
#define	IA1C		( I1C + mC * 0.75*h*0.75*h )
#define	ddpsiC	( ( I3C - 2.0*IA1C ) * dpsi*cos(theta) + I3C*dphi ) * dtheta / ( IA1C * sin(theta) )
#define	ddthetaC	( mC*_g*0.75*h*sin(theta) + dpsi*sin(theta) * ( (IA1C-I3C)*dpsi*cos(theta) - I3C*dphi) ) / IA1C
#define	ddphiC	( ( IA1C - ( I3C-IA1C )*cos(theta)*cos(theta) )*dpsi - I3C*dphi*cos(theta) ) * dtheta / ( IA1C * sin(theta) )

#include "Toupie.h"

namespace ProjetEPFL
{

class ConeSimple : public Toupie
{
	public :
		
		//	Constructeurs
		ConeSimple () ;
		ConeSimple ( double MV, double H, double R ) ;
		
		//	Méthodes
		Vecteur equationEvolution ( double t ) const ;
		Vecteur equationEvolution ( double t, Vecteur om, Vecteur dom ) const ;
		void affiche ( std::ostream &out ) const ;
		virtual void dessine ( SortieDessin sortie = ECRAN ) const ;
		virtual Toupie* copie () const ;

		inline double m () const ;
			
	protected :

		double h ;				//	Hauteur
		double r ;				//	Rayon
};

//	Opérateurs externes
std::ostream &operator<< ( std::ostream &out, ConeSimple &cone ) ;

double deg ( double angle ) ;
double rad ( double angle ) ;

}

#endif	//PROJETEPFL_CONESIMPLE_H_INCLUDED
