#include "Chinoise.h"

namespace ProjetEPFL
{

//	Constructeurs

Chinoise::Chinoise () : /*Toupie::*/Toupie()/*:dom(0.1,0.15,6.0)*/, h(0.2), r(1.5)
{
	dom = Vecteur(0.1,0.15,6.0) ;
	mV = 0.1 ;
}

Chinoise::Chinoise( double MV, double H, double R)
: Toupie(), h(H), r(R)
{
	mV = MV ;
}

//	Méthodes

Vecteur Chinoise::equationEvolution ( double t ) const
{
	return Vecteur ( ddpsiCh, ddthetaCh, ddphiCh, ddchiCh, ddxsiCh ) ;
}

Vecteur Chinoise::equationEvolution ( double t, Vecteur om, Vecteur dom ) const
{
	return Vecteur() ;
}

void Chinoise::affiche ( std::ostream &out ) const
{
	out << "Toupie chinoise : " << std::endl << 
		 ">Rayon     : " << r << std::endl << 
		 ">Hauteur   : " << h << std::endl << 
		 ">Masse Vol : " << mV << std::endl ;
	
	Toupie::affiche(out) ;
}

void Chinoise::dessine ( SortieDessin sortie ) const
{
	 Toupie::affiche(std::cout) ;
}

Toupie* Chinoise::copie () const
{
	return new Chinoise(*this) ;
}



//	Opérateurs externes

std::ostream &operator<< ( std::ostream &out, Chinoise &chine )
{
	chine.affiche(out) ;
	
	return out ;
}

}
