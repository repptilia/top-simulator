#ifndef SRC_BASIC_VEC3
#define SRC_BASIC_VEC3

/**
 * Specific vector of size 3, used for positions/speeds.
 */

#include <boost/numeric/ublas/vector.hpp>

using namespace boost::numeric::ublas;

class vec3
{
	public:
		vec3(double v1, double v2, double v3);
		vec3(vector<double> v);
		double norm_2() const;

		// Cross-product
		vec3 operator^(const vec3& vec) const;
		double operator[](int i) const;

		vector<double> getVector() const;


	protected:
		vector<double> m_vec;
};

#endif // SRC_BASIC_VEC3 