#ifndef SRC_BASIC_MAT3
#define SRC_BASIC_MAT3

/**
 * Specific vector of size 3, used for positions/speeds.
 */

#include <boost/numeric/ublas/matrix.hpp>
#include "vec3.hpp"

using namespace boost::numeric::ublas;

class mat33
{
	public:
		mat33 (double v1, double v2, double v3);
	//	mat33 (double a, double b, double c, double d, double e, double f, double g, double h, double i ) ;
		
		//	Méthodes
	//	mat33 diag() const;
	//	mat33 tr() const;
	//	double det() const;
	//	mat33 inv () const;

		vec3 operator*(const vec3& v) const;
		double& operator()(unsigned int i, unsigned int j);

	protected:
		matrix<double> m_mat;
};

#endif // SRC_BASIC_MAT3 