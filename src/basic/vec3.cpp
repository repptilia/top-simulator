#include "vec3.hpp"

vec3::vec3(double v1, double v2, double v3)
{
	m_vec = vector<double>(3);

	m_vec[0] = v1;
	m_vec[1] = v2;
	m_vec[2] = v3;
}

vec3::vec3(vector<double> v)
{
	m_vec[0] = v[0];
	m_vec[1] = v[1];
	m_vec[2] = v[2];
}

double vec3::norm_2() const
{
	return boost::numeric::ublas::norm_2(m_vec);
}

vec3 vec3::operator^(const vec3& vec) const
{
	return vec3(
		m_vec[1]*vec[2]-m_vec[2]*vec[1],
		m_vec[2]*vec[0]-m_vec[0]*vec[2],
		m_vec[0]*vec[1]-m_vec[1]*vec[0]
	);
}

vector<double> vec3::getVector() const
{
	return m_vec;
}

double vec3::operator[](int i) const
{
	return m_vec[i];
}