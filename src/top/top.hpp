#ifndef SRC_TOP_TOP
#define SRC_TOP_TOP

#include <boost/numeric/ublas/vector.hpp>
#include "../basic/vec3.hpp"

using namespace boost::numeric::ublas;

class Top
{
	public:

		Top();

		/**
		 * Equation of evolution
		 */
		virtual vect3 evolution_equation();	

	protected:
		double mass;

		/**
		 * Euler angles
		 */
		double euler_phi;
		double euler_psi;
		double euler_theta;

		vect3 g_pos;
		vect3 g_speed;

		vect3 ang_speed;

		double coeff_i1;
		double coeff_i3;

};

#endif // SRC_TOP_TOP
