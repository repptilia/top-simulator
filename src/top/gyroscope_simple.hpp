#ifndef SRC_TOP_GYROSCOPE_SIMPLE
#define SRC_TOP_GYROSCOPE_SIMPLE

/**
 * GyroscopeSimple
 *
 * This particular top can be expressed only in function of ang_speed and coeff_i1, coeff_i3.
 * Its tip is fixed to the ground.
 */

#include "top.hpp"

using namespace boost::numeric::ublas;

class GyroscopeSimple : public Top
{
	public:

		Top();

		/**
		 * Equation of evolution
		 */
		void evolution_equation();	

	protected:

};

#endif // SRC_TOP_GYROSCOPE_SIMPLE
