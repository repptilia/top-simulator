#ifndef TOP_SIMULATOR_INTEGRATOR_INTEGRATOR
#define TOP_SIMULATOR_INTEGRATOR_INTEGRATOR

#include "top/top.hpp"

class Integrator
{
	public :
		Integrator() : m_dt(1e-3);
		Integrator(double dt) : m_dt(dt);
		
		double dt() const;

		virtual void computeNextStep(Top* top, double t) const = 0;

	protected :
		double m_dt ;
};

#endif // TOP_SIMULATOR_INTEGRATOR_INTEGRATOR

