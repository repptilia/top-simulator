#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test_framework.hpp>
#include <iostream>
#include "basic/mat33.hpp"

BOOST_AUTO_TEST_CASE( my_test )
{
	mat33 m1(1,2,3);

	std::cout << m1(2,2) << std::endl;
}