#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test_framework.hpp>
#include "basic/vec3.hpp"

BOOST_AUTO_TEST_CASE( my_test )
{
	// Check operator[]
	vec3 v1(1,2,3);

	BOOST_CHECK(v1[0] == 1);
	BOOST_CHECK(v1[1] == 2);
	BOOST_CHECK(v1[2] == 3);

	// Check norm
	vec3 v2(1,0,0);
	BOOST_CHECK(v2.norm_2() == 1);

	vec3 v3(3,4,0);
	BOOST_CHECK(v3.norm_2() == 5);

	// Check vector product
	vec3 v4(0,1,0);
	vec3 v5(v2^v4);
	vec3 v6(0,0,1);

	BOOST_CHECK(v5[0] == v6[0]);
	BOOST_CHECK(v5[1] == v6[1]);
	BOOST_CHECK(v5[2] == v6[2]);
}